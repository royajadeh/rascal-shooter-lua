local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- Global vars set up
_W = display.contentWidth;
_H = display.contentHeight;
display.setStatusBar( display.HiddenStatusBar )
m = {}
m.random = math.random;

local state = display.newGroup();

--Imports
local movieclip = require("movieclip");
local physics = require "physics"
physics.start();
local projectile = require("projectile");


-- Variables setup
local projectiles_container = nil;
local force_multiplier = 10;
local velocity = m.random(50,100);


function scene:createScene( event )
local group = self.view

--Calling Background
local background = display.newImageRect( "images/bg.jpg", display.contentWidth, display.contentHeight )
background:setReferencePoint( display.TopLeftReferencePoint )
background.x, background.y = 0, 0

-- Calling Catapult
local slingshot_strut_back = display.newImage("images/catapult.png",true);
slingshot_strut_back.x = 350;
slingshot_strut_back.y = _H - 75;




group:insert( background )
group:insert( slingshot_strut_back)
end
	


--calling bajaj animation
local function1, function2


local Bajaj = movieclip.newAnim{ "images/b1.png", "images/b2.png", "images/b3.png", "images/b4.png", "images/b5.png", "images/b6.png" }
Bajaj:play{ startFrame=1, endFrame=0, loop=0, remove=true }
Bajaj.y=_H-475;
Bajaj.x=1015;
local w,h = display.contentWidth, display.contentHeight


local function resetpeach (e)
Bajaj.y=_H-475;
Bajaj.x=1015;
local w,h = display.contentWidth, display.contentHeight
transition.to( Bajaj, { time=2500, x=-500, y=(h-475), onComplete=function1} )
end

function function1(e)
transition.to( Bajaj, { time=2500, x=-500, y=(h-475), onComplete=resetpeach} )
end

 

transition.to( Bajaj, { time=2500, delay= 1500, x=-500, y=(h-475), onComplete=resetpeach} )




	
-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )
	
return scene