local js = {}
-----------------------------------------------------------------------------------------
--                                        js.lua                                       --
--  Usage:                                                                             --
--      js = require( "js" )                                                           --
--                                                                                     --
-----------------------------------------------------------------------------------------

local rdmSeed = math.randomseed
local min = math.min
local max = math.max
local floor = math.floor
local ceil = math.ceil
local rdm = math.random

rdmSeed(os.time())

------------------------------------------------------------------------------------------
--                                       js.memCheck()                                  --
--  Usage:  checks memory usage                                                         --
--returns:  prints to terminal                                                          --
--   code:  js.memCheck() prints memory usage to the terminal							--
------------------------------------------------------------------------------------------
        js.memCheck = function()
            collectgarbage("collect")
            useMem = floor( ( collectgarbage("count") * (10^2) ) + 0.5 ) / 10^2
            txtMem = floor( ( ( system.getInfo( "textureMemoryUsed" ) / 1048576 ) * (10^2) ) + 0.5 ) / 10^2
            print( "\n\n--------------------------------------------------" )
            print(   "--    Memory Usage            Texture Memory    --" )
            print(   "--------------------------------------------------" )
            print(   "        "..useMem.."KB                   ".. txtMem.."MB" )
            print( "\n\n" )
        end

------------------------------------------------------------------------------------------
--                                       js.memRepeat( arg1 )                           --
--  Usage:  checks memory usage                                                         --
--          arg1 = number of seconds to repeat                                          --
--returns:  prints to terminal                                                          --
--   code:  js.memRepeat( 4 ) will reprint memory usage to the terminal every 4 seconds --
--          js.memRepeat( 0 ) will cancel the repeat                                    --
------------------------------------------------------------------------------------------
         js.memRepeat = function( arg1 )

            if arg1 == 0 and memTimer ~= nil then
                timer.cancel( memTimer )
                memTimer = nil
            else
                arg1 = ( min( 15, max( 0, arg1 ) ) * 1000 )
                memTimer = timer.performWithDelay( arg1, memCheck, -1 )
            end

        end

------------------------------------------------------------------------------------------
--                                       js.highscoreAdd( arg1, arg2, arg3 )            --
--  Usage:  adds a name and score to the local highscore list                           --
--          arg1 = table name                                                           --
--          arg2 = new score                                                            --
--          arg3 = new name                                                             --
--   code:  js.highscoreAdd( "highscores", 1400, "jeff")								--
--			see file api below for example of setting up the highscores table			--
------------------------------------------------------------------------------------------
         js.highscoreAdd = function( arg1, arg2, arg3 )
            local a = #arg1 - 1
            print( a )
            repeat
                a = a - 2
                print( arg1[a], arg2)
            until a == 0 or arg2 < tonumber(arg1[a])
            print("inserting at "..a )
            table.insert( arg1, (a+1), arg3 )
            table.insert( arg1, (a+2), arg2 )
            table.remove( arg1, #arg1 - 1 )
            table.remove( arg1, #arg1 )
            return arg1
        end

------------------------------------------------------------------------------------------
--                                       js.fileInit( arg1, arg2 )                      --
--  Usage:  checks for file if no file found creates file                               --
--          arg1 = fileName                                                             --
--          arg2 = table                                                                --
--   code:  local fileKey = { settings = { 2, 6, 10, 3 },								--
--							highscores = { "empty",0,"empty",0,"empty", 0 },			--
--							gamedata = { 1, 3 } }										--
--			basically you can save any info just set a name and enter the info			--
--			then																		--
--			js.fileInit( "game.dat", fileKey )											--
--			put the fileKey anywhere at begining of code where you set up variables		--
--			for your app to start, then call js.fileInit( arg1, arg2 ) ' leave the		--
--			.dat off if you want to be able to check the data in the file from the		--
--			app sandbox' this function checks to see if this is the first run and		--
--			if i is it save the fileKey to the filename you supply if not the first		--
--			run it does nothing															--	
------------------------------------------------------------------------------------------
         js.fileInit = function( arg1, arg2 )
            print( arg1 )
            print( arg2 )
            local path = system.pathForFile( arg1, system.DocumentsDirectory )
            local fh = io.open( path, "r" )
            if fh then
                print( "File Found" )
                io.close( fh )
            else
                print( "No File Found" )
                fh = io.open( path, "w" )
                print( "writing..." )
                for k,v in pairs( arg2 ) do
                    fh:write( k.."\n")
                    print( k.."\n" )
                    for i,t in ipairs(v) do
                        if i < #v then
                            print(i, t..",")
                            fh:write( t.."," )
                        else
                            fh:write( t )
                        end
                    end
                    fh:write("\n")
                    print( "\n" )
                end
                io.close( fh )
            end
        end

------------------------------------------------------------------------------------------
--                                       js.fileSave( arg1, arg2 )                      --
--  Usage:  to save file                                                                --
--          arg1 = fileName                                                             --
--          arg2 = table                                                                --
--   code:  js.fileSave( "game.dat", settings )											--
--			when something changes and you need to save it, say you need to save just	--
--			the settings you would do game.settings={5,3,10,1}							--
-- 			then call js.fileSave														--
------------------------------------------------------------------------------------------
         js.fileSave = function( arg1, arg2 )
            print( arg1 )
            print( arg2 )
            local path = system.pathForFile( arg1, system.DocumentsDirectory )
            local fh = io.open( path, "r" )
            if fh then
                print( "File Found" )
                fh = io.open( path, "w" )
                print( "writing..." )
                for k,v in pairs( arg2 ) do
                    fh:write( k.."\n")
                    print( k.."\n" )
                    for i,t in ipairs(v) do
                        if i < #v then
                            print(i, t..",")
                            fh:write( t.."," )
                        else
                            fh:write( t )
                        end
                    end
                    fh:write("\n")
                    print( "\n" )
                end
                io.close( fh )
            else
                print( "No file found" )
            end
        end

------------------------------------------------------------------------------------------
--                                       js.fileLoad( arg1 )                            --
--  Usage:  to load file                                                                --
--          arg1 = fileName                                                             --
--returns:  table                                                                       --
--   code:  game = js.fielLoad( "game.dat" )											--
------------------------------------------------------------------------------------------
         js.fileLoad = function( arg1 )
            local path = system.pathForFile( arg1, system.DocumentsDirectory )
            local fh = io.open( path, "r" )
            local value1 = ""
            local tab, pos, a, b = {}, 0, 0, 0
            if fh then

                for line in fh:lines() do
                print(line)
                    if string.find( line, "," ) == nil then
                        tab[ line ] = {}
                        value1 = line
                        line = nil
                        a, b, pos = 0, 0, 0
                    else
                        for a, b in function() return string.find( line, ",", pos, true ) end do
                            print( "inserting..."..string.sub( line, pos, a-1 ) )
                            table.insert( tab[value1], string.sub( line, pos, a - 1 ) )
                            pos = b + 1
                        end
                        print( "inserting..."..string.sub( line, pos ) )
                        table.insert( tab[value1], string.sub( line, pos ) )
                    end
                end
            end
            for k,v in pairs(tab) do
            print(k)
                for i,t in ipairs( v ) do
                print("    "..i,t)
                end
            end
            io.close( fh )
            return tab
        end


-----------------------------------------------------------------------------------------
--                             js.collisionCheck( obj1, obj2 )                         --
--  Usage:  to detcet collision between two objects                                    --
--returns:  check = true or false                                                      --
--          arg1 ={ x1 = ??, y1 = ??, w1 = ??, h1 = ?? }                               --
--          arg2 ={ x2 = ??, y2 = ??, w2 = ??, h2 = ?? }                               --
-----------------------------------------------------------------------------------------
         js.collisionCheck = function( arg1, arg2 )
            if arg1.x1 > arg2.x2 + arg2.w2 - 1 or
               arg1.y1 > arg2.y2 + arg2.h2 - 1 or
               arg2.x2 > arg1.x1 + arg1.w1 - 1 or
               arg2.y2 > arg1.y1 + arg1.h1 - 1 then
                return false
            else
                return true
            end
        end

-----------------------------------------------------------------------------------------
--                                  js.lowWeighted( arg1, arg2, arg3 )                 --
--  Usage:  returns a random on the low end                                            --
--returns:  number                                                                     --
--          arg1 = lowest random number                                                --
--          arg2 = highest random number                                               --
--          arg3  = any number 2+      higher number results in more low numbers       --
--	 code:  rdmNumber = js.lowWeighted( 1, 10, 3 )									   --	
-----------------------------------------------------------------------------------------
         js.lowWeighted = function( arg1, arg2, arg3 )
            num = floor(arg1+(arg2-arg1)*rdm()^arg3)
            return num
        end

-----------------------------------------------------------------------------------------
--                                  js.highWeighted( arg1, arg2, arg3 )                --
--  Usage:  returns a random on the high end                                           --
--returns:  number                                                                     --
--          arg1 = lowest random number                                                --
--          arg2 = highest random number                                               --
--          arg3  = any number 2+      higher number results in more high numbers      --
--	 code:  rdmNumber = js.highWeighted( 1, 100, 2 )								   --	
-----------------------------------------------------------------------------------------
         js.highWeighted = function( arg1, arg2, arg3 )
            num = ceil(arg2-(arg2-arg1)*rdm()^arg3)
            return num
        end

------------------------------------------------------------------------------------------

return js

