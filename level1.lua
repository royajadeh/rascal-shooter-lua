local storyboard = require( "storyboard" )
local movieclip = require("movieclip");
local physics = require("physics");
local projectile = require("projectile");
require "sprite"
physics.start();
physics.setGravity (0,0)

display.setStatusBar( display.HiddenStatusBar )

--variable setup 
m = {}
m.random = math.random;
local projectiles_container = nil;
local force_multiplier = 10;
local velocity = m.random(50,100);

local scene = storyboard.newScene()
_W = display.contentWidth;
_H = display.contentHeight;


local destroyBullet = function(e)

	e:removeSelf()
end
local hit = audio.loadSound('resources/sounds/hit.wav')
local state = display.newGroup();
-- Visible groups setup
local background = display.newGroup();
local bg_image = display.newImage("resources/images/level01/l01-background.jpg",true);
background:insert(bg_image);

local awan2 = display.newGroup();
local awan2 = display.newImage("resources/images/level01/l01-awan2.png", true)
awan2.y=250;
awan2.x=1000

local function resetpeach2 (e)
awan2.y=250;
awan2.x=1000
transition.to( awan2, { time=55500, delay=10000, x=-100, y=250, onComplete=function12} )
end

function function12(e)
transition.to( awan2, { time=55500,delay=10000, x=-100, y=250, onComplete=resetpeach2} )
end

transition.to( awan2, { time=55500,delay=10000, x=-100, y=250,  onComplete=resetpeach2} );

local awan = display.newGroup();
local awan = display.newImage("resources/images/level01/l01-awan1.png", true)
awan.y=180;
awan.x=1000;

local function resetpeach (e)
awan.y=180;
awan.x=1000;
transition.to( awan, { time=50500, x=-100, y=180, onComplete=function1} )
end

function function1(e)
transition.to( awan, { time=50500, x=-100, y=180, onComplete=resetpeach} )
end

transition.to( awan, { time=50500, x=-100, y=180,  onComplete=resetpeach} );

local warung = display.newGroup();
local warung = display.newImage( "resources/images/level01/l01-warungpohon.png", true );
warung.y = 450;

local slingshot_strut_back = display.newImage("resources/images/ketapel.png",true);
slingshot_strut_back.x =_W/2;
slingshot_strut_back.y = 840;

local slingshot_container = display.newGroup();
local function1, function2



local bajaj_sound = audio.loadSound("resources/sounds/bajaj.mp3");
local Bajaj = movieclip.newAnim{ "resources/images/b1.png", "resources/images/b2.png", "resources/images/b3.png", "resources/images/b4.png", "resources/images/b5.png", "resources/images/b6.png" }
Bajaj:play{ startFrame=1, endFrame=0, loop=0, remove=true }
Bajaj.y=_H-405;
Bajaj.x=1015;
local w,h = display.contentWidth, display.contentHeight


local function resetbajaj (e)
Bajaj.y=_H-405;
Bajaj.x=1015;
local w,h = display.contentWidth, display.contentHeight
transition.to( Bajaj, { time=2500, x=-500, y=(h-405), onComplete=function1} )
-- audio.play(bajaj_sound);
end

function function1(e)
transition.to( Bajaj, { time=2500, x=-500, y=(h-405), onComplete=resetbajaj} )
-- audio.play(bajaj_sound);
end

 

transition.to( Bajaj, { time=2500, delay= 1500, x=-500, y=(h-405), onComplete=resetbajaj} )
-- audio.play(bajaj_sound);

local baseline = 280
local sheet2 = sprite.newSpriteSheet( "resources/images/jkt/humanoid.png", 300, 300 )

local spriteSet2 = sprite.newSpriteSet(sheet2, 1, 25)
sprite.add( spriteSet2, "man", 1, 25, 1400, 0 ) -- play 9 frames every 200 ms

local instance2 = sprite.newSprite( spriteSet2 )
instance2.x = 900
instance2.y = 590
local shapeOrang = {-32,-100, 12,-100, 12,-95, -32,-95  }
local orang = {density = 0.3, friction=1, bounce=0, isSensor = false, shape=shapeOrang }
physics.addBody(instance2,"static",  orang );
instance2:prepare("man")
instance2:play()
-- physics.setDrawMode( "hybrid" ) 

local function resetbatik (e)
instance2.x = 900
instance2.y = 590

transition.to( instance2, { time=7700, x=-10, y=590, onComplete=functionbatik} )
end

function functionbatik(e)
instance2.x = 900
instance2.y = 590
transition.to( instance2, { time=7700, x=-10, y=590, onComplete=resetbatik} )
end

transition.to( instance2, { time=7700, x=-10, y=590, onComplete=resetbatik} )



 
-- Move catapult up
slingshot_container.y = -25;
 
local state_value = nil;
 
-- Audio
local shot = audio.loadSound("resources/sounds/band-release.aif");
local band_stretch = audio.loadSound("resources/sounds/stretch.mp3");
 
-- Transfer variables to the projectile classes
projectile.shot = shot;
projectile.band_stretch = band_stretch;


--[[

projectile TOUCH FUNCTION

]]--

local function projectileTouchListener(e)
	

-- The current projectile on screen
	
local t = e.target;
	

-- If the projectile is 'ready' to be used

if(t.ready) then
		

-- if the touch event has started...

if(e.phase == "began") then
	

-- Play the band stretch
			
audio.play(band_stretch);
			
-- Set the stage focus to the touched projectile
			
display.getCurrentStage():setFocus( t );

t.isFocus = true;

t.bodyType = "kinematic";
			
			

-- Stop current physics motion, if any
			
t:setLinearVelocity(0,0);
			
t.angularVelocity = 0;
			
			

-- Init the elastic band.
			
local myLine = nil;

local myLineBack = nil;
			
		
-- Bunny eyes animation
			

		

		
-- If the target of the touch event is the focus...
		
elseif(t.isFocus) then
			

-- If the target of the touch event moves...
	
if(e.phase == "moved") then
				


-- If the band exists... refresh the drawing of the line on the stage.
				
if(myLine) then
			myLine.parent:remove(myLine); 

-- erase previous line
					
myLineBack.parent:remove(myLineBack); -- erase previous line
					
myLine = nil;
					
myLineBack = nil;
				
end
							
			
-- If the projectile is in the top left position
				
if(t.x < _W/2 and t.y < _H - 385)then
		
myLine = display.newLine(t.x - 30, t.y, 630, _H - 355);
						
myLineBack = display.newLine(t.x - 40, t.y, 160, _H - 352);

-- Insert the components of the catapult into a group.
                                slingshot_container:insert(slingshot_strut_back);
                                slingshot_container:insert(myLineBack);
                                slingshot_container:insert(t);
                                slingshot_container:insert(myLine);
				

-- If the projectile is in the top right position
                                elseif(t.x > _W/2 and t.y < _H - 385)then
                                        myLine= display.newLine(t.x + 30, t.y - 5, 630, _H - 355);
                                        myLineBack  = display.newLine(t.x + 40, t.y - 5, 160, _H - 352);
										
										-- Insert the components of the catapult into a group.
                                slingshot_container:insert(slingshot_strut_back);
                                slingshot_container:insert(myLine);
                                slingshot_container:insert(t);
                                slingshot_container:insert(myLineBack);		
										
                                -- If the projectile is in the bottom left position
                                elseif(t.x < _W/2 and t.y > _H - 385)then
                                        myLine = display.newLine(t.x - 25, t.y + 20, 616, _H - 355);
                                        myLineBack = display.newLine(t.x - 25, t.y + 20, 160, _H - 352);
										slingshot_container:insert(slingshot_strut_back);
                                slingshot_container:insert(myLineBack);
                                slingshot_container:insert(t);
                                slingshot_container:insert(myLine);	
										
                                -- If the projectile is in the bottom right position
                                elseif(t.x > _W/2 and t.y > _H - 385)then
                                        myLine = display.newLine(t.x + 5, t.y + 20, 616, _H - 355);
                                        myLineBack = display.newLine(t.x + 35, t.y + 15, 160, _H - 352);
										
								slingshot_container:insert(slingshot_strut_back);
                                slingshot_container:insert(myLine);
                                slingshot_container:insert(t);
                                slingshot_container:insert(myLineBack);	
                                
								elseif(t.x == _W/2)then
                                        myLine = display.newLine(t.x + 5, t.y + 10, 616, _H - 355);
                                        myLineBack = display.newLine(t.x + 8, t.y + 15, 160, _H - 352);
										
								slingshot_container:insert(slingshot_strut_back);
                                slingshot_container:insert(t);
								slingshot_container:insert(myLine);
                                slingshot_container:insert(myLineBack);	
								
                                else
								
                                -- Default position (just in case).
                                        myLine = display.newLine(t.x - 25, t.y + 20, 616, _H - 355);
                                        myLineBack = display.newLine(t.x - 25, t.y + 20, 160, _H - 352);
                                end
                                
                                -- Set the elastic band's visual attributes
                                myLineBack:setColor(214,184,130);
                                myLineBack.width = 10;
                                
                                myLine:setColor(243,207,134);
                                myLine.width = 10;
                                
                                
                                
                                
                                
                                -- Boundary for the projectile when grabbed                     
                                local bounds = e.target.stageBounds;
                                bounds.xMax = 800;
							
                                bounds.yMax = _H - 200;
                                
                                if(e.y < bounds.yMax) then
                                        t.y = e.y;
                                else
                                
                                end
                                
                                if(e.x < bounds.xMax) then
                                        t.x = e.x;
                                else
                                        -- Do nothing
                                end
								
							
								
							
                        
                        -- If the projectile touch event ends (player lets go)...
                        elseif(e.phase == "ended" or e.phase == "cancelled") then
                        
                                -- Open bunny eyes
                               
                                -- Remove projectile touch so player can't grab it back and re-use after firing.
                                projectiles_container:removeEventListener("touch", projectileTouchListener);
                                -- Reset the stage focus
                                display.getCurrentStage():setFocus(nil);
                                t.isFocus = false;
                                
                                -- Play the release sound
                                audio.play(shot);
                                
                                -- Remove the elastic band
                                if(myLine) then
                                        myLine.parent:remove(myLine); -- erase previous line
                                        myLineBack.parent:remove(myLineBack); -- erase previous line
                                        myLine = nil;
                                        myLineBack = nil;
                                end
                                
                                -- Launch projectile
                                t.bodyType = "dynamic";
                                t:applyForce((360 - e.x)*force_multiplier, (_H - 385 - e.y)*force_multiplier, t.x, t.y);
                                t:applyTorque( 100 )
                                t.isFixedRotation = false;
								t.xScale = 2
								t.yScale = 2
								distance = math.sqrt(((_W/2)-t.x)^2+((_H - 300)-t.y)^2)
								transition.to (t, {time=distance+5, rotation=-380, xScale=0.4, yScale=0.4, transition=easing.outExpo, onComplete=destroyBullet} )
								
								
								print ("jarak" .. tonumber(distance))
								
								
								
								local function onLocalCollision( self, event )
        if ( event.phase == "ended" ) then
 audio.play(hit)
                print( "Im Hit" )
 
       
 
        end
end
 
instance2.collision = onLocalCollision
instance2:addEventListener( "collision", instance2 )
        
t.collision = onLocalCollision
t:addEventListener( "collision", t )
								
								
                                                                
                                -- Wait a second before the catapult is reloaded (Avoids conflicts).
                                t.timer = timer.performWithDelay(1000, function(e)
                                state:dispatchEvent({name="change", state="fire"});
                                
                                if(e.count == 1) then
                                        timer.cancel(t.timer);
                                        t.timer = nil;
                                end

                                
                                end, 1)
                                        
                        end		
		end
	
	end

end

--[[

SPAWN projectile FUNCTION

]]--
local function spawnProjectile()

	-- If there is a projectile available then...
	if(projectile.ready)then
	
		projectiles_container = projectile.newProjectile();
		-- Flag projectiles for removal
		projectiles_container.ready = true;
		projectiles_container.remove = true;
		
		-- Reset the indexing for the visual attributes of the catapult.
		slingshot_container:insert(instance2);
		slingshot_container:insert(Bajaj);
		slingshot_container:insert(slingshot_strut_back);
		
		 
		slingshot_container:insert(projectiles_container);
		
		
		-- Reset bunny eyes animation
		
		
		-- Add an event listener to the projectile.
		projectiles_container:addEventListener("touch", projectileTouchListener);
		
	end

end
--[[

GAME STATE CHANGE FUNCTION

]]--
function state:change(e)

	if(e.state == "fire") then
	
		-- You fired...
		-- new projectile please
		spawnProjectile();
			
	end

end




-- Tell the projectile it's good to go!

projectile.ready = true;


-- Spawn the first projectile.

spawnProjectile();

-- Create listnener for state changes in the game

state:addEventListener("change", state);

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------



return scene
