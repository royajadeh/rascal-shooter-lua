local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local movieclip = require "movieclip"
local physics = require 'physics'
local sprite = require "sprite"
local audio = require "audio"

physics.start()
--physics.setDrawMode("debug")

-- Basic Setup
local W, H = display.contentWidth, display.contentHeight
local background = display.newGroup()
--physics.setGravity (0, 19.8)

-- Basic Screen
local bgImg = display.newImage('resources/images/bg.jpg',true);
background:insert(bgImg);

local function onBulletPreCollision (self, event)
	print( "OnBulletPreCollision ".. self.name)
	other = event.other

	if other and other.name then
		print("Other object ".. other.name)

		if other.name == "subject" then
			--subject.isSensor = true
		end
	end
end

createBullet = function()
	local bullet = display.newImage('resources/images/ammo/bun.png')
	bullet.name = "bullet"
	bullet.x = (W / 2) - (bullet.width/2)
	bullet.y = H-65
	bullet:setFillColor(245, 132, 0);
  bulletShape = { -10,-10, 10,-10, 10,10, -10,10}
	physics.addBody (bullet, {friction=1, density=1, bounce=0, shape=bulletShape})
	--bullet.preCollision = onBulletPreCollision
	--bullet:addEventListener ("preCollision", bullet)
	return bullet
end

launchBullet = function(params)
	--subject.isSensor = false
	--transition.to (bullet, {time=2000, y=400, rotation=-180, xScale=0.5, yScale=0.5, transition=easing.outExpo, onComplete=resetBullet} )
	--bullet:applyLinearImpulse(0, -1, bullet.x, bullet.y/2)
	bullet = createBullet()
	--bullet.x = params.x
	bullet.xScale = 3
	bullet.yScale = 3
	bullet:resetMassData()
	bullet:applyForce((params.x - (W/2))*1.7, -570, bullet.x, bullet.y/2)
	transition.to (bullet, {time=2550, norotation=math.random(-360, 360), xScale=0.2, yScale=0.2, transition=easing.outExpo, onComplete=destroyBullet} )
end




resetBullet = function()
	bullet.xScale = 2
	bullet.yScale = 2
	bullet.y = H-30
	bullet.rotation = 0
	launchBullet()
end

destroyBullet = function(e)
  if e == nil then return nil end
	print "destroy bullet"
	print(e)
	e:removeSelf()
	e = nil
end



local hit = audio.loadSound('resources/sounds/hit.wav')

--
subject = display.newImage('resources/images/crate.png', 320, 350)
subjectHitShape = { -30,-5, 30,-5, 30,0, -30,0}
physics.addBody (subject, "static", {friction=1, density=0.3, bounce=0, isSensor = false, shape=subjectHitShape})
subject.name = "subject"
--subject.isSensor = true

local function onSubjectCollision (self, event)
	print ("On Subject Collision ".. event.phase)
	other = event.other

	if other and other.name then
		print("Other object ".. other.name)

		if event.phase == 'began' and other.name == "bullet" then
      audio.play(hit)
      print("IM HITTT");
			--physics.removeBody(other)
			--destroyBullet(other)
			--subject.isSensor = true
			--other.isSensor = true
		end
	end

end

subject.collision = onSubjectCollision
subject:addEventListener("collision", subject)





--[[
local subject = sprite.newSpriteSheet('resources/images/jkt/batik.png', 300, 300)
sprite.add( subject, "man", 1, 8, 1400, 0 ) -- play 9 frames every 200 ms
subject.x = 100
subject.y = 460
subject:play()
]]

local bajajImg = movieclip.newAnim{ "resources/images/b1.png", "resources/images/b2.png", "resources/images/b3.png", "resources/images/b4.png", "resources/images/b5.png", "resources/images/b6.png" }
bajajImg.x = W+500
bajajImg.y = H/2
bajajImg:play()
background:insert(bajajImg)

transition.to (bajajImg, {time=8000, x=-800})

local function onTap( event )
	print( "Tap name: " .. event.name )
	print( "Tap: event(" .. event.numTaps .. ") ("..event.x..","..event.y..")" )
	--resetBullet()
	launchBullet(event)
end

Runtime:addEventListener( "tap", onTap)

return scene
